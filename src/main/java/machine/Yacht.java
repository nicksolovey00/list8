package machine;

public class Yacht extends Ship{
    public final String model;

    public Yacht(int amountOfMast, String serialNumber) {
        super(amountOfMast, serialNumber);
        model = "Yacht";
    }

    public String getModel() {
        return model;
    }
}
