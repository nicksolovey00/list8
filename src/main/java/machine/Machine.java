package machine;

public interface Machine {
    void move();

    void stop();
}
