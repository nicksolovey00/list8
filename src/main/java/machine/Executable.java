package machine;

public interface Executable {
    void execute();
}
