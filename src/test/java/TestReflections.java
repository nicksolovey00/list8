import machine.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class TestReflections {
    @Test
    public void testAmountOfCarClassInArray(){
        Object[] objects = new Object[]{
                new Car(500, "1234", "Green"),
                new BMW(30, "jg", "Black"),
                new Yacht(33, "44343j"),
                3, 55.0
        };

        Assert.assertEquals(2, Reflections.amountOfCarClassInArray(objects));
    }

    /* Филиппов А.В. 29.11.2020 Комментарий не удалять.
     Не работает. Плохой тест. Что он проверяет?
    */
    @Test
    public void testGetMethodsForObject(){
        Car car = new Car(500, "1234", "green");
        Set<String> expected = new HashSet<>();
        Collections.addAll(expected,
                "setHorsePowers", "getClass", "getSerialNumber", "move", "wait", "setSerialNumber", "notifyAll",
                "getColor", "execute", "notify", "getHorsePowers", "setColor", "stop", "hashCode", "equals", "toString"
        );

        Set<String> res = new HashSet<>();
        for (String s: Reflections.getMethodsForObject(car)){
            res.add(s);
        }
        Assert.assertEquals(expected, res);
    }

    /* Филиппов А.В. 29.11.2020 Комментарий не удалять.
     Не работает.
    */
    /* Филиппов А.В. 30.11.2020 Комментарий не удалять.
     Не работает по прежнему.
     У Opel есть открытый метод getHorsePowers - это легко проверить написав car.getHorsePowers()
     Тогда тут будут все открытые методы вплоть до Object
    */
    @Test
    public void testGetMethodsForObject2(){
        Opel car = new Opel(500, "1234", "green");

        Set<String> expected = new HashSet<>();
        Collections.addAll(expected,
                "setHorsePowers", "getClass", "getModel", "getSerialNumber", "move", "wait", "setSerialNumber",
                "notifyAll", "getColor", "execute", "notify", "getHorsePowers", "setColor", "stop", "hashCode", "equals", "toString"
        );

        Set<String> res = new HashSet<>();
        for (String s: Reflections.getMethodsForObject(car)){
            res.add(s);
        }
        Assert.assertEquals(expected, res);
    }

    @Test
    public void testGetAllSuperClasses(){
        BMW_E34 bmw = new BMW_E34(30, "jg", "Black");
        List<String> expected = new ArrayList<>();
        Collections.addAll(expected, "BMW", "Car", "Object");
        List<String> res = new ArrayList<>();
        for (String s: Reflections.getAllSuperClasses(bmw)){
            res.add(s);
        }
        Assert.assertEquals(expected, res);
    }

    @Test
    public void testGetAmountOfExecObjects() throws InvocationTargetException, IllegalAccessException {
        Object[] objects = new Object[]{
                //new Car(500, "1234", "Green"),
                new BMW(30, "jg", "Black"),
                new Opel(55, "jjj3", "Blue"),
                new Yacht(33, "44343j"),
                3, 55.0
        };

        Assert.assertEquals(1, Reflections.getAmountOfExecObjects(objects));
    }

    @Test
    public void testGetSettersList(){
        Car car = new Car(500, "1234", "green");
        Set<String> expected = new HashSet<>();
        Collections.addAll(expected, "setSerialNumber", "setHorsePowers", "setColor");

        Set<String> res = new HashSet<>();
        for (String s: Reflections.getSettersList(car)){
            res.add(s);
        }
        Assert.assertEquals(expected, res);
    }

    @Test
    public void testGetGettersList(){
        Car car = new Car(500, "1234", "green");
        Set<String> expected = new HashSet<>();
        Collections.addAll(expected, "getSerialNumber", "getHorsePowers", "getColor");

        Set<String> res = new HashSet<>();
        for (String s: Reflections.getGettersList(car)){
            res.add(s);
        }
        Assert.assertEquals(expected, res);
    }


}
